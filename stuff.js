function http_get(url, { "User-Agent" = "kitchen sink 2.0" }, { sayHi, verbose: v }) {
	if (sayHi) {
		console.log("HI!");
	}

	if (v) {
		console.log("Sending request");
	}

	return require('http').get({
		url,
		headers: arguments[1]
	});
}

http_get("http://icanhazip.com", { "Accept": "love, gzip", "User-Agent": "NotTheKitchenSink" }, { true, sayHi: false });
